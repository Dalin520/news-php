<?php
class Db
{	
    private $db_host = "localhost";
	private $db_user = "root"; 
	private $db_pass = "root"; 
	private $db_name = "news";
	public $cnn;
		
	public function connect()   
	{   
		$this->cnn = new mysqli($this->db_host, $this->db_user, $this->db_pass,$this->db_name);		
		$this->cnn->set_charset('utf8');
		// Check connection
		if ($this->cnn->connect_error) 
		{
    		die("Connection failed: " . $this->cnn->connect_error);
		} 
	}
	
	
}
?>
