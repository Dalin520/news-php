<?php
// $cn = new mysqli('localhost','root','root','news');
include('../_config_inc.php');
include(BASE_PATH.'/db_connection/database.php');
$db=new Db;
$db->connect();
include(BASE_PATH.'/fragments/adm_head.php');
$sql = "SELECT * FROM tbl_news_type ORDER BY id DESC";
$result = $db->cnn->query($sql);
$num = $result->num_rows;
if($num==0)
{
    $id=1;
}
else{
    $row= $result->fetch_array();
    $id = $row[0]+1;
}
?>

</head>
<body>
    <?php
        include(BASE_PATH.'/fragments/adm_menu.php')
    ?>
    <div class="container-fluid">
        <button type="button" id="btn_new" class="btn btn-info mt-3 mb-3"><i class="fa fa-plus"></i> &nbsp;បង្កើតថ្មី</button>
        <div class='row'>
            <div class="col-lg-12">
                <!-- Table display data -->
                <table class="table table-bordered table-light" id="tbl_data">
                    <thead class="thead-light">
                        <tr>
                            <th scope="col" width="150px">លេខរៀង</th>
                            <th scope="col">ឈ្មោះ</th>
                            <th scope="col" width="100px">រូបភាព</th>
                            <th scope="col" width="100px">ស្ថានភាព</th>
                            <th scope="col" width="50px">សកម្មភាព</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            $sql = "SELECT * FROM tbl_news_type ORDER BY id DESC";
                            $result = $db->cnn->query($sql);
                            while($row=$result->fetch_array())
                            {
                                ?>
                                    <tr>
                                        <td><?php echo $row[0]; ?></td>
                                        <td><?php echo $row[1]; ?></td>
                                        <td>
                                            <input type="hidden" value="<?php echo $row[2]; ?>">
                                            <img src="../img/news/<?php echo $row[2]; ?>" width="50"/>
                                        </td>
                                        <td>
                                            <?php
                                                if($row[3]==1)
                                                {
                                                ?>
                                                    <img src="../img/active.png" width="40" data-no="<?php echo $row[3]; ?>" data-id="<?php echo $row[0]; ?>" class="img_st">
                                                <?php
                                                }
                                                else
                                                {
                                                ?>
                                                    <img src="../img/inactive.png" width="40" data-no="<?php echo $row[3]; ?>" data-id="<?php echo $row[0]; ?>" class="img_st">
                                                <?php
                                                }
                                            ?>
                                        </td>
                                        <td>
                                            <button class="btn btn-success" id="btn_edit"><i class="fa fa-pencil"></i></button>
                                        </td>
                                    </tr>
                                <?php
                            }
                        ?>
                    </tbody>
                </table>
            </div>        
        </div>
    </div>
    <!-- Post form -->
    <div class='post-frm p-4'>
        <form class='upl' method="post" enctype="multipart/form-data">
            <input type="hidden" id="txt_edit_no" name="txt_edit_no" value="0" >
            <label>No</label>
            <input type="text" class='form-control' readonly id='txt_no' name='txt_no' value="<?php echo $id;?>">
            <label>Name</label>
            <input type="text" class='form-control' id='txt_name' name='txt_name' autofocus>
            <label>Photo</label><br/>
            <div class="box-img">
                <input type="file" id='txt_file' name='txt_file'>
            </div>
            <input type="hidden" id="txt_img" name="txt_img">
            <br/>
            <input type="submit" value="Post" class="btn btn-success" id="btn_post" />
        </form>
    </div>
    <!-- end Post form -->
    <script src="../script/jQuery/jquery-3.3.1.js"></script>
    <script src="../script/bootstrap/bootstrap.js"></script>
    <script src="../script/popper/popper.js"></script>
    <script>
        $(document).ready(function(){
            // set img status
            $('.img_st').mouseover(function(){
                var eThis=$(this);
                var no=eThis.data('no');
                var id=eThis.data('id');
                if(no==1)
                {
                    eThis.attr('src','../img/inactive.png');
                }
                else
                {
                    eThis.attr('src','../img/active.png');	
                }
                
            });
            
            $('.img_st').mouseout(function(){
                var eThis=$(this);
                var no=eThis.data('no');
                if(no==1)
                {
                    eThis.attr('src','../img/active.png');
                }
                else
                {
                    eThis.attr('src','../img/inactive.png');
                }
            });
            $('.img_st').click(function(){
                var eThis=$(this);
                var no=eThis.data('no');
                var id=eThis.data('id');
                $.ajax({
                    url:'actions/del-at-type.php',
                    type:'POST',
                    data:{no:no,id:id},
                    cache:false,
                    //dataType:"json",
                    success:function(data)
                    {
                        if(no==1)
                        {
                            eThis.attr('src','../img/active.png');
                            eThis.data("no","2");
                        }
                        else
                        {
                            eThis.attr('src','../img/inactive.png');	
                            eThis.data("no","1");
                        }		
                    }
                    
                })
            });
            // end set img status

            var ind;
            //edit data
            $('#tbl_data').on('click','#btn_edit',function(){
                var tr = $(this).closest('tr');
                var no = tr.find('td:eq(0)').text();
                var name = tr.find('td:eq(1)').text();
                var img = tr.find('td:eq(2) input').val();
                //ind is the index of table
                ind = tr.index()+1;
                $('#txt_no').val(no);
                $('#txt_name').val(name);
                $('#txt_img').val(img); 
                $('.box-img').css({'background-image':'url("../img/news/'+img+' ")'});
                // check if user click on btn edit then change form's status number for modifying form
                $('#txt_edit_no').val(no);

                var postfrm = $('.post-frm')
                var pop='<div class="pop-frm"><div class="close-frm" id="close-frm"><i class="fa fa-close text-white"></i></div></div>'
                $('body').append(pop)
                $('body').find('.pop-frm').append(postfrm);
                postfrm.show();
            })

            //add photo file
            $('#txt_file').change(function(){
                var parent=$(this).parent();
                var frm=$(this).closest('form.upl');
                var frm_data=new FormData(frm[0]);
                $.ajax({
                    url:'./actions/upload_img.php',
                    type:'POST',
                    data:frm_data,
                    contentType:false,
                    cache:false,
                    processData:false,
                    dataType:"json",
                    success:function(data)
                    {
                        var img=data.img_name;
                        parent.css({'background-image': 'url("../img/news/'+img+'")'});
                        $('#txt_img').val(img);
                    }
                });
            });

            // when click on button add new
            $('#btn_new').click(function(){
                $.ajax({
                    url:'./actions/num_rows.php',
                    contentType:false,
                    cache:false,
                    processData:false,
                    dataType:"json",
                    success:function(data)
                    {
                        $('#txt_no').val(data.id);
                    }
                });
                var postfrm = $('.post-frm')
                var pop='<div class="pop-frm"><div class="close-frm" id="close-frm"><i class="fa fa-close text-white"></i></div></div>'
                $('body').append(pop)
                $('body').find('.pop-frm').append(postfrm);
                postfrm.show();
                // check if user click on btn add new that val = 0 to confirm the form is for adding new
                $('#txt_edit_no').val(no);
                
                
            })
            // When click button close inside form
            $('body').on('click','.close-frm',function(){
                var postfrm = $('.post-frm')
                $('body').append(postfrm)
                $('body').find(postfrm).hide()
                $(this).parent().remove()
                // remove data form input field before form will being closed
                $('#txt_edit_no').val(0)
                $('#txt_name').val('')
                $('#txt_name').focus();
                $('.box-img').css({'background-image':'url("../img/page/bg-default-.png")'});
                $('#txt_img').val('');
                $('.box-img').find('input').val('');
            })
            // submit data
            $('#btn_post').click(function(e){
                e.preventDefault()
                var name = $('#txt_name');
                var edit_no = $('#txt_edit_no')
                if(name.val()== ''){
                    alert('Please input name');
                    name.focus()
                }
                else{
                    var boxImg = $('.box-img');
                    var frm=$(this).closest('form.upl');
                    var frm_data= new FormData(frm[0]);
                    $.ajax({
                        url:'./actions/save_news_type.php',
                        type:'POST',
                        data:frm_data,
                        contentType:false,
                        cache:false,
                        processData:false,
                        dataType:"json",
                        success:function(data)
                        {
                            // for add data when form's status equal Zero
                            if(edit_no.val()==0)
                            {
                                if(data.dplname==1)
                                {
                                    alert('The name have already existed.');
                                    name.focus();
                                }
                                else{
                                    $('#txt_no').val(data.id);
                                    add_data();
                                    name.val('')
                                    name.focus();
                                    boxImg.css({'background-image':'url("../img/page/bg-default-.png")'});
                                    $('#txt_img').val('');
                                    boxImg.find('input').val('');
                                }
                            }
                            // for updating form
                            else{
                                edit_tbl_data();
                                var postfrm = $('.post-frm')
                                $('body').append(postfrm)
                                $('body').find(postfrm).hide()
                                $('body').find('.pop-frm').remove();

                            }
                        }
                    })
                }
            });
            //edit table data
            function edit_tbl_data()
            {
                var tbl = $('#tbl_data');
                var name = $('#txt_name').val();
                var img = $('#txt_img').val();
                tbl.find('tr:eq('+ind+') td:eq(1)').text(name);
                tbl.find('tr:eq('+ind+') td:eq(2) input').val(img);
                tbl.find('tr:eq('+ind+') td:eq(2) img').attr('src','../img/news/'+img+'');
            }

            //add table data
            function add_data(){
                var no = $('#txt_no').val();
                var name = $('#txt_name').val();
                var img = $('#txt_img').val();
                var tbl = document.getElementById('tbl_data');
                rows = tbl.getElementsByTagName('tr').length;
                var row = tbl.insertRow(1);
                var cell1 = row.insertCell(0);
                var cell2 = row.insertCell(1);
                var cell3 = row.insertCell(2);
                var cell4 = row.insertCell(3);
                var cell5 = row.insertCell(4);
                cell1.innerHTML = no-1;
                cell2.innerHTML = name;
                cell3.innerHTML = '<img src="../img/news/'+img+'"width="50"/>';
                cell4.innerHTML = 1;
                cell5.innerHTML = ' <button class="btn btn-success" id="btn_edit"><i class="fa fa-pencil"></i></button>';
            }
        })
    </script>
</body>
</html>
