<?php
// $cn = new mysqli('localhost','root','root','news');
include('../_config_inc.php');
include(BASE_PATH.'/db_connection/database.php');
$db=new Db;
$db->connect();
include(BASE_PATH.'/fragments/adm_head.php');
$sql = "SELECT * FROM tbl_img ORDER BY id DESC";
$result = $db->cnn->query($sql);
$num = $result->num_rows;
if($num==0)
{
    $id=1;
}
else{
    $row= $result->fetch_array();
    $id = $row[0]+1;
}
?>

</head>
<body>
    <?php
        include(BASE_PATH.'/fragments/adm_menu.php')
    ?>
    <div class="container-fluid">
        <button type="button" id="btn_new" class="btn btn-info mt-3 mb-3"><i class="fa fa-plus"></i> &nbsp;បង្កើតថ្មី</button>
        <div class='row'>
            <div class="col-lg-12">
                <!-- Table display data -->
                <table class="table table-bordered table-light" id="tbl_data">
                    <thead class="thead-light">
                        <tr>
                            <th scope="col" width="150px">លេខរៀង</th>
                            <th scope="col">ឈ្មោះ</th>
                            <th scope="col" width="100px">រូបភាព</th>
                            <th scope="col" width="100px">ស្ថានភាព</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            $sql = "SELECT * FROM tbl_img ORDER BY id DESC";
                            $result = $db->cnn->query($sql);
                            while($row=$result->fetch_array())
                            {
                                ?>
                                    <tr>
                                        <td><?php echo $row[0]; ?></td>
                                        <td><?php echo $row[1]; ?></td>
                                        <td>
                                            <input type="hidden" value="<?php echo $row[2]; ?>">
                                            <img src="../img/news/<?php echo $row[2]; ?>" width="50"/>
                                        </td>
                                        <td><?php echo $row[3]; ?></td>
                                    </tr>
                                <?php
                            }
                        ?>
                    </tbody>
                </table>
            </div>        
        </div>
    </div>
    <!-- Post form -->
    <div class='post-frm p-4'>
        <form class='upl' method="post" enctype="multipart/form-data">
            <input type="hidden" id="txt_edit_no" name="txt_edit_no" value="0" >
            <label>No</label>
            <input type="text" class='form-control' readonly id='txt_no' name='txt_no' value="<?php echo $id;?>">
            <label>Name</label>
            <input type="text" class='form-control' id='txt_name' name='txt_name' autofocus>
            <label>Photo</label><br/>
            <div class="box-img">
                <input type="file" id='txt_file' name='txt_file'>
            </div>
            <input type="hidden" id="txt_img" name="txt_img">
            <br/>
            <input type="submit" value="Post" class="btn btn-success" id="btn_post" />
        </form>
    </div>
    <!-- end Post form -->
    <script src="../script/jQuery/jquery-3.3.1.js"></script>
    <script src="../script/bootstrap/bootstrap.js"></script>
    <script src="../script/popper/popper.js"></script>
    <script>
        $(document).ready(function(){
            var ind;

            //add photo file
            $('#txt_file').change(function(){
                var parent=$(this).parent();
                var frm=$(this).closest('form.upl');
                var frm_data=new FormData(frm[0]);
                $.ajax({
                    url:'./actions/upload_img.php',
                    type:'POST',
                    data:frm_data,
                    contentType:false,
                    cache:false,
                    processData:false,
                    dataType:"json",
                    success:function(data)
                    {
                        var img=data.img_name;
                        parent.css({'background-image': 'url("../img/news/'+img+'")'});
                        $('#txt_img').val(img);
                    }
                });
            });

            // when click on button add new
            $('#btn_new').click(function(){
                $.ajax({
                    url:'./actions/num_rows_img.php',
                    contentType:false,
                    cache:false,
                    processData:false,
                    dataType:"json",
                    success:function(data)
                    {
                        $('#txt_no').val(data.id);
                    }
                });
                var postfrm = $('.post-frm')
                var pop='<div class="pop-frm"><div class="close-frm" id="close-frm"><i class="fa fa-close text-white"></i></div></div>'
                $('body').append(pop)
                $('body').find('.pop-frm').append(postfrm);
                postfrm.show();
                
            })
            // When click button close inside form
            $('body').on('click','.close-frm',function(){
                var postfrm = $('.post-frm')
                $('body').append(postfrm)
                $('body').find(postfrm).hide()
                $(this).parent().remove()
                // remove data form input field before form will being closed
                $('#txt_edit_no').val(0)
                $('#txt_name').val('')
                $('#txt_name').focus();
                $('.box-img').css({'background-image':'url("../img/page/bg-default-.png")'});
                $('#txt_img').val('');
                $('.box-img').find('input').val('');
            })
            // submit data
            $('#btn_post').click(function(e){
                e.preventDefault()
                var name = $('#txt_name');
                var edit_no = $('#txt_edit_no')
                if(name.val()== ''){
                    alert('Please input name');
                    name.focus()
                }
                else{
                    var boxImg = $('.box-img');
                    var frm=$(this).closest('form.upl');
                    var frm_data= new FormData(frm[0]);
                    $.ajax({
                        url:'./actions/save-img.php',
                        type:'POST',
                        data:frm_data,
                        contentType:false,
                        cache:false,
                        processData:false,
                        dataType:"json",
                        success:function(data)
                        {
                            // for add data when form's status equal Zero
                            $('#txt_no').val(data.id);
                            add_data();
                            name.val('')
                            name.focus();
                            boxImg.css({'background-image':'url("../img/page/bg-default-.png")'});
                            $('#txt_img').val('');
                        }
                    })
                }
            });

            //add table data
            function add_data(){
                var no = $('#txt_no').val();
                var name = $('#txt_name').val();
                var img = $('#txt_img').val();
                var tbl = document.getElementById('tbl_data');
                rows = tbl.getElementsByTagName('tr').length;
                var row = tbl.insertRow(1);
                var cell1 = row.insertCell(0);
                var cell2 = row.insertCell(1);
                var cell3 = row.insertCell(2);
                var cell4 = row.insertCell(3);
                cell1.innerHTML = no-1;
                cell2.innerHTML = name;
                cell3.innerHTML = '<img src="../img/news/'+img+'"width="50"/>';
                cell4.innerHTML = 1;
            }
        })
    </script>
</body>
</html>
