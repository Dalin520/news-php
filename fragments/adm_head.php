<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Admin Page</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href='https://fonts.googleapis.com/css?family=Angkor|Battambang' rel='stylesheet'>
    <link rel="stylesheet" href="../styles/bootstrap/bootstrap-4.1.3.css" />
    <link rel="stylesheet" href="../styles/adm_style.css" />
    <script src="../script/jQuery/jquery-3.3.1.js"></script>