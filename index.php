<?php
include('./_config_inc.php');
include('./db_connection/database.php');
$db=new Db;
$db->connect();
    include('./fragments/head.php'); 
?>

</head>
<body>
    <div class="container-fluid">
        <input type="hidden" id="wis" value=""><!-- don't delete this line because it is return the menu display status -->
        <!-- start top bar -->
        <div class="header">
            <div class="container-fluid bg-black">
                <div class="container">
                    <!-- top-banner which container logo -->
                    <div class="top-banner">
                        <div class="logo-wrapper">
                            <img src="./img/news-logo1.png" alt="news website">
                        </div>
                    </div>
                </div>
            </div>
            <div class="btn-menu">
                <div class="menu-div"></div>
                <div class="menu-div"></div>
                <div class="menu-div"></div>
            </div>
            <div class="container-fluid bg-red">
                <div class="container">
                    <!-- menu with phone menu -->
                    <div class="menu-wrapper">
                        <ul>
                            <li><a href="index.php"><i class="fa fa-home"></i></a></li>
                            <?php
                                // opacity to check active top menu by its opacity 0.6 or 1 which is selected
                                $opacity=0.6;
                                $sql = "SELECT * FROM tbl_news_type WHERE status=1";
                                $result = $db->cnn->query($sql);
                                while($row=$result->fetch_array())
                                {
                                    if(isset($_GET['type']))
                                    {
                                        $type=$_GET['type'];
                                        if($type==$row[0]) // it is true when type's value equal to menu type which is selected
                                        {
                                            $opacity=1;
                                        }
                                        else{
                                            $opacity=0.6;
                                        }
                                    }
                                    ?>
                                        <li><a href="index.php?type=<?php echo $row[0]; ?>" style='opacity:<?php echo $opacity; ?>' ><?php echo $row[1]; ?></a></li>
                                    <?php
                                }
                                // 
                            ?>
                        </ul>
                    </div>
                </div>
            </div> 
        </div>
        <!-- end top bar -->
        <!-- start content body -->
        <div class='content'>
            <div class='container'>
                <div class="wrapper-box-9">
                <?php
                    // check if detail news
                    if(isset($_GET['news']))
                    {
                        $id= $_GET['news'];

                        // Count when news var path is existed (COUNT VISITORS)
                        $sql_count = "UPDATE tbl_news SET view=view+1 WHERE id='".$id."' ";
                        $db->cnn->query($sql_count); 

                        // query data to display in detail 
                        $sql = "SELECT * FROM tbl_news WHERE id='".$id."' ";
                        $result = $db->cnn->query($sql);
                        $row = $result->fetch_array();
                    ?>
                        <div class="wrapper-box-content">
                            <div class="content-card box-shadow">
                                <div class="content-title">
                                    <h1><?php echo $row[4]; ?></h1>
                                </div>
                                <div class="content-view">
                                    <p>ចំនួនអ្នកទស្សនា: <?php echo $row[7]; ?></p>
                                </div>
                                <div class="content-img-wrapper">
                                    <center><img src="img/news/<?php echo $row[6]; ?>" alt="Image not found"></center>
                                </div>
                                <div class="content-des">
                                    <p><?php echo $row[5]; ?></p>
                                </div>
                            </div>
                        </div>
                    <?php
                    }
                    else
                    {
                        // check if get news type
                        if(isset($_GET['type']))
                        {
                            $type = $_GET['type'];
                            $sql = "SELECT * FROM tbl_news WHERE status=1 AND type='".$type."' ORDER BY id DESC ";

                        }
                        else
                        {
                            $sql = "SELECT * FROM tbl_news WHERE status=1 ORDER BY id DESC ";
                        }
                        $result = $db->cnn->query($sql);
                        while($row = $result->fetch_array())
                        {
                            ?>
                            <!-- section Listing each news boxes -->
                            <div class="bg-news">
                                <a href="index.php?news=<?php echo $row[0]; ?>">
                                    <div class='box-shadow news-box'>
                                        <div class="wrapper-img wrapper-box-3 news-img" 
                                            style="background-image:url('./img/news/<?php echo $row[6]; ?>')">
                                        </div>
                                        <div class='wrapper-box-des'>
                                            <div class="wrapper-des">
                                                <a href="index.php?news=<?php echo $row[0]; ?>"><?php echo $row[4];?></a>
                                                <p><?php echo mb_substr($row[5],0,100);?>...</p>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <?php
                        }
                    }
                ?>
                </div>
                <!-- section advertisement and other -->
                <div class="wrapper-box-content-3" style="background-color:whitesmoke;">
                    <div class="wrapper-ads">
                        <center>
                            <img src="./img/ads/ads1.gif" alt="" style="width:100%;">
                        </center>
                    </div>
                    <div class="wrapper-ads">
                        <center>
                            <img src="./img/ads/ads2.gif" alt="" style="width:100%;">
                        </center>
                    </div>
                </div>
                <!-- end section advertisement and other -->
            </div>
        </div>
        <!-- end content body -->
    </div>


    <!-- script -->
    <script src='./script/bootstrap/bootstrap.js'></script>
    <script src='./script/popper/popper.js'></script>
    <script src="./script/jQuery/jquery-3.3.1.js"></script>
    <script src="./script/myScript/script.js"></script>
</body>
</html>